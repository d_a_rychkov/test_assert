#include "task.h"
#include <QDebug>

Task::Task(QObject* parent)
    : QObject(parent)
{

}

void Task::run()
{
    int a;
    for (int i = 0; i < 12; ++i) {
        a = factorial(i);
        qDebug() << "factorial of " << i << " = " << a;
    }

    emit finished();
}

// Считает факториал числа n.
// Число n должно лежать в пределах от 0 до 10 включительно.
int Task::factorial(int n)
{
  // Факториал отрицательного числа не считается
  Q_ASSERT(n >= 0); //assert(n >= 0);

  // Если n превысит 10, то это может привести либо к целочисленному
  // переполнению результата, либо к переполнению стэка.
  Q_ASSERT(n <= 10); //assert(n <= 10);

  if (n < 2) {
    return 1;
  }

  return factorial(n - 1) * n;
}
