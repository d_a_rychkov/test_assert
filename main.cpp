#include <QCoreApplication>
#include "task.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Task* task = new Task();
    task->run();

    return a.exec();
}
